import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { ObjectService } from '../../../services/object.service';
import { StoreObjectService } from '../../../services/store-object.service';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss']
})
export class FilesComponent implements OnInit {
  bucket = this.storeObjectService.getBucket();
  pickedFile: any;
  filesList: any = [];

  constructor(private objectService: ObjectService, private storeObjectService: StoreObjectService, public router: Router) { }

  ngOnInit(): void {
    this.getFiles();
  }

  getFiles(): void {
    this.filesList = [];
    this.objectService.getObjects().subscribe((data: any[]) => {
      for (const object of data) {
        // tslint:disable-next-line:triple-equals
        if (object.bucketId == this.bucket.id) {
          this.filesList[this.filesList.length] = object;
          this.pickedFile = object;
        }
      }
    }, (error: any[]) => console.log(error));
  }

  picked(file: any): void {
    this.pickedFile = file;
  }

  delete(): void {
    if (this.pickedFile != undefined) {
      if (confirm('Do you really want to delete file ' + this.pickedFile.name + '?')) {
        this.objectService.deleteObject(this.pickedFile.id).subscribe((data: any[]) => {
          this.getFiles();
        }, (error: any[]) => console.log(error));
      } else {
        console.log('NE briši');
      }
    }
    else {
      console.log("NO FILES TO DELETE");
    }
  }

  upload(): void {
    // @ts-ignore
    document.getElementById('file-input').click();
  }

  addUnit(size: any): string {
    if (size / 1000000000000 >= 1) {
      size = Math.round(size / 1000000000000) + ' TB';
    }
    else if (size / 1000000000 >= 1) {
      size = Math.round(size / 1000000000) + ' GB';
    }
    else if (size / 1000000 >= 1) {
      size = Math.round(size / 1000000) + ' MB';
    }
    else if (size / 1000 >= 1) {
      size = Math.round(size / 1000) + ' KB';
    }
    else {
      size += ' B';
    }
    return size;
  }

  fileAdded(): void {
    let fileObject;
    let file = document.getElementById('file-input');
    // @ts-ignore
    let size = file.files[0].size;
    size = this.addUnit(size);
    // @ts-ignore
    const date = new Date(file.files[0].lastModified);
    // @ts-ignore
    fileObject = { file: file.files[0], name: file.files[0].name, size: size, 'lastModified': date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear(), bucketId: this.bucket.id };
    this.saveObject(fileObject);
  }

  saveObject(object: any): any {
    this.objectService.storeObject(object).subscribe((data: any[]) => {}, (error: any[]) => console.log(error));
    this.getFiles();
  }

}
