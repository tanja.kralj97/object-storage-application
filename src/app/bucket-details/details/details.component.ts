import { Component, OnInit } from '@angular/core';
import { StoreObjectService } from '../../../services/store-object.service';
import { ObjectService } from '../../../services/object.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  storageSize: any = 0;
  filesList: any = [];
  bucket: any;
  constructor(private storeObjectService: StoreObjectService, private objectService: ObjectService) { }

  ngOnInit(): void {
    this.bucket = this.storeObjectService.getBucket();
    this.objectService.getObjects().subscribe((data: any[]) => {
      for (const object of data) {
        // tslint:disable-next-line:triple-equals
        if (object.bucketId == this.bucket.id) {
          this.filesList[this.filesList.length] = object;
        }
      };
      this.getStorageSize();
    }, (error: any[]) => console.log(error));
  }

  getFromUnit(object: any): number {
    let i = 1;
    if (object.size.split(' ')[1] == 'KB') {
      i = 1000;
    }
    else if (object.size.split(' ')[1] == 'MB') {
      i = 1000000;
    }
    else if (object.size.split(' ')[1] == 'GB') {
      i = 1000000000;
    }
    else if (object.size.split(' ')[1] == 'TB') {
      i = 1000000000000;
    }
    return i;
  }

  getToUnit(): void {
    if (this.storageSize / 1000000000000 >= 1) {
      this.storageSize = Math.round(this.storageSize / 1000000000000) + ' TB';
    }
    else if (this.storageSize / 1000000000 >= 1) {
      this.storageSize = Math.round(this.storageSize / 1000000000) + ' GB';
    }
    else if (this.storageSize / 1000000 >= 1) {
      this.storageSize = Math.round(this.storageSize / 1000000) + ' MB';
    }
    else if (this.storageSize / 1000 >= 1) {
      this.storageSize = Math.round(this.storageSize / 1000) + ' KB';
    }
    else {
      this.storageSize += ' B';
    }
  }

  getStorageSize(): void {
    let i = 1;
    // sum file sizes - rounded values
    for (let object of this.filesList) {
      i = this.getFromUnit(object);
      this.storageSize += (object.size.split(' ')[0]) * i;
    }
    // turn size in easier readable format
    this.getToUnit();
  }
}
