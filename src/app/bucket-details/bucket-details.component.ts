import { Component, OnInit } from '@angular/core';
import { StoreObjectService } from '../../services/store-object.service';
import {Router} from '@angular/router';
import { BucketService } from '../../services/bucket.service';


@Component({
  selector: 'app-bucket-details',
  templateUrl: './bucket-details.component.html',
  styleUrls: ['./bucket-details.component.scss']
})
export class BucketDetailsComponent implements OnInit {

  displayFiles: boolean = true;
  displayDetails: boolean = false;
  bucket: any;

  constructor(public router: Router, private storeObjectService: StoreObjectService, private bucketService: BucketService) { }

  ngOnInit(): void {
    this.bucket = this.storeObjectService.getBucket();
    if (this.bucket === undefined) {
      this.router.navigateByUrl('');
    }
  }

  displayContent(content: string): void {
    if (content === 'files') {
      this.displayFiles = true;
      this.displayDetails = false;
    }
    else {
      this.displayFiles = false;
      this.displayDetails = true;
    }
  }

  deleteBucket(): void {
    if (confirm('Do you really want to delete this bucket?')) {
      this.bucketService.deleteBucket(this.bucket.id).subscribe((data: any[]) => {
      }, (error: any[]) => console.log(error));
      this.router.navigateByUrl('');
    }
    // else {
    //   console.log('NE briši');
    // }
  }

}
