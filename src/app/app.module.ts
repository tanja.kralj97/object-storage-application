import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BucketListComponent } from './bucket-list/bucket-list.component';
import { BucketNewComponent } from './bucket-list/bucket-new/bucket-new.component';
import {FormsModule} from '@angular/forms';
import { BucketDetailsComponent } from './bucket-details/bucket-details.component';
import { FilesComponent } from './bucket-details/files/files.component';
import { DetailsComponent } from './bucket-details/details/details.component';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from '../services/in-memory-data.service';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpInterceptor} from '@angular/common/http';
import { TokenInterceptorService } from '../services/token-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    BucketListComponent,
    BucketNewComponent,
    BucketDetailsComponent,
    FilesComponent,
    DetailsComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        InMemoryWebApiModule.forRoot(InMemoryDataService)
    ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
