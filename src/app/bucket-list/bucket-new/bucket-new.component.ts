import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BucketService } from '../../../services/bucket.service';
import { LocationService } from '../../../services/location.service';

@Component({
  selector: 'app-bucket-new',
  templateUrl: './bucket-new.component.html',
  styleUrls: ['./bucket-new.component.scss']
})
export class BucketNewComponent implements OnInit {

  @Output() saveEvent = new EventEmitter<boolean>();
  warrning: boolean = false;
  locations: any = [];
  name: string = "";
  location: string = "";

  constructor(private bucketService: BucketService, private locationService: LocationService) { }

  ngOnInit(): void {
    this.locationService.getLocation().subscribe((data: any[]) => {
      this.locations = data;
    }, (error: any[]) => console.log(error));
  }

  createBucket(): void {
    if (this.name != "" && this.locations != "") {
      let bucket = {name: this.name, location: this.location};
      this.bucketService.saveBucket(bucket).subscribe((data: any[]) => {
        this.saveEvent.emit(true);
      }, (error: any[]) => console.log(error));
    }
    else {
      this.warrning = true;
    }
  }

}
