import { Component, OnInit } from '@angular/core';
import { StoreObjectService } from '../../services/store-object.service';
import { BucketService } from '../../services/bucket.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-bucket-list',
  templateUrl: './bucket-list.component.html',
  styleUrls: ['./bucket-list.component.scss']
})
export class BucketListComponent implements OnInit {
  visibilityNewBucket: boolean = false;
  bucketList: any = [];

  constructor(public router: Router, private storeObjectService: StoreObjectService, private bucketService: BucketService) { }

  ngOnInit(): void {
    this.bucketService.getBuckets().subscribe((data: any[]) => {
      this.bucketList = data;
    }, (error: any[]) => console.log(error));

  }

  newBucket(): void {
    this.visibilityNewBucket = true;
  }

  setVisibility($event: any): void {
    this.visibilityNewBucket = false; // ALI KDAJ PUSTIŠ KOT TRUE?
    this.bucketService.getBuckets().subscribe((data: any[]) => {
      this.bucketList = data;
    }, (error: any[]) => console.log(error));
  }

  navigate(bucket: any): void {
    this.storeObjectService.setBucket(bucket);
    this.router.navigateByUrl('details');
  }

}
