import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BucketService {

  SERVER_URL = 'storage/buckets';
  constructor(private httpClient: HttpClient) { }

  public getBuckets(): any {
    return this.httpClient.get(this.SERVER_URL);
  }

  public getBucket(id: any): any {
    return this.httpClient.get(this.SERVER_URL + '/' + id);
  }

  public saveBucket(bucket: any): any {
    return this.httpClient.post(this.SERVER_URL, bucket);
  }

  public deleteBucket(id: any): any {
    return this.httpClient.delete(this.SERVER_URL + '/' + id);
  }

}
