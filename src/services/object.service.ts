import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ObjectService {

  SERVER_URL = 'storage/objects';
  constructor(private httpClient: HttpClient) { }

  public getObjects(): any {
    return this.httpClient.get(this.SERVER_URL);
  }

  public deleteObject(id: any): any {
    return this.httpClient.delete(this.SERVER_URL + '/' + id);
  }

  public storeObject(object: any): any {
    return this.httpClient.post(this.SERVER_URL, object);
  }

}
