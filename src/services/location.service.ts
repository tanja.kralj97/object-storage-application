import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  SERVER_URL = 'storage/locations';
  constructor(private httpClient: HttpClient) { }

  getLocation(): any {
    return this.httpClient.get(this.SERVER_URL);
  }
}
