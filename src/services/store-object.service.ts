import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StoreObjectService {

  bucket: any;

  constructor() { }

  setBucket(bucket: any): void {
    this.bucket = bucket;
  }

  getBucket(): any {
    return this.bucket;
  }
}
