import { TestBed } from '@angular/core/testing';

import { StoreObjectService } from './store-object.service';

describe('StoreObjectService', () => {
  let service: StoreObjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StoreObjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
