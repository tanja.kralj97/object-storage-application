import { Injectable } from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() { }

  createDb(): any{

    let  buckets =  [
      {  id:  1, name: 'BestStorage', location: 'Kranj' },
      {  id:  2, name: 'Pics', location: 'Ljubljana' }
    ];

    let locations = [
      {  id:  1, location: 'Kranj' },
      {  id:  2, location: 'Ljubljana' }
    ];

    let objects = [
      { id:  1, name: 'Filename01', lastModified: '06.09.2015', size: '2 MB', bucketId: '1', file: null },
      { id:  2, name: 'Filename02', lastModified: '05.01.2015', size: '100 KB', bucketId: '1', file: null },
      { id:  3, name: 'Filename03', lastModified: '12.11.2016', size: '2 MB', bucketId: '1', file: null },
    ];

    return {buckets, locations, objects};

  }
}
